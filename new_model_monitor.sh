#!/usr/bin/env bash
inotifywait -m --exclude "[^h][^5]$" ./src/features -e create -e moved_to |
    while read path action file; do
        echo "The file ''$file' appeared in directory ''$path' via '$action'"
    done