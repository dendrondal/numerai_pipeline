from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Pipeline for numerai data which automatically pulls data from api, does etl, and submits cookiecutter models to the weekly competition',
    author='Jon Steven Dal Williams',
    license='MIT',
)
