import numerox as nx
from keras.models import load_model
from scipy.optimize import minimize
from sklearn.metrics import accuracy_score
import numpy as np
import os, re

# Path-to-model-location should be included in the makefile.
MODEL_LOCATION = '../../models'
DATA_LOCATION = '../../data/raw/numerai_dataset.hdf'


def load_validation():
    full_data = nx.load_data(DATA_LOCATION)
    validation = full_data['validation']
    return validation.x, validation.y


def load_keras_models():
    keras_models = dict()
    pattern = re.compile('.*h5$')
    for i, file in enumerate(os.listdir(MODEL_LOCATION)):
        if re.match(pattern, file):
            keras_models[f'mlp_{i}'] = mlp(file)
    return keras_models


def model_aggregation(max_num):
    skl_models = []
    #Placeholder, eventually sklearn pickles will added here ala keras
    keras_models = load_keras_models()
    if max_num > len(skl_models) + len(keras_models):
        max_num = len(skl_models) + len(keras_models)
        print(f"Too many models specified, trimming down to {max_num} combos")
    staged = np.random.choice(keras_models, np.random.choice(range(max_num+1), 1))
    X, y = load_validation()
    preds = []
    for model in staged:
        preds.append(load_model(model).predict(X))
    final_preds = [1/len(staged) * pred for pred in preds]
    #for now, a brute force approach is being taken. Later,
    #Nedler-Mead will be used to optimize coefficients.
    print(accuracy_score(y.values, final_preds))
    return final_preds


class mlp(nx.Model):
    """Numerox constructor that allows for custom typing,
    along with easy ensembling and summary reporting"""
    def __init__(self, model_name):
        self.name = f'{MODEL_LOCATION}/{model_name}'

    def fit_predict(self, dfit, dpre, tournament):
        model = load_model(self.name)
        model.fit(dfit.x, dfit.y[tournament])
        yhat = model.predict_proba(dpre.x)[:, 1]
        return dpre.ids, yhat


def ensembling():
    models = list(load_keras_models().values())
    data = nx.load_data(DATA_LOCATION)
    predictions = list()
    for model in models:
        predictions.append(nx.production(model, data, 'bernie'))
    return predictions














