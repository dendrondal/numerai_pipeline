from src.features.build_features import \
    TRAIN, training_generation, PROJECT_DIR, era_sampling
from tpot import TPOTClassifier
from sklearn.ensemble import RandomForestClassifier


def tpot_classifier():
    """Brute force fitting with TPOT"""
    data = era_sampling(TRAIN, 30)
    X_train, X_test, y_train, y_test = training_generation(data)
    tpot = TPOTClassifier(generations=5, population_size=8,
                          verbosity=2)
    tpot.fit(X_train, y_train)
    print(tpot.score(X_train, y_train))
    tpot.export(PROJECT_DIR + 'models/bernie_pipeline_01.py')
    return tpot


def RF_mode

#if __name__ == '__main__':
#    main()