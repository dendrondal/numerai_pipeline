from keras.layers import Input, Dense, BatchNormalization, Dropout
from keras.models import Model
from keras import callbacks, regularizers, optimizers
from keras import backend as K
import numpy as np
from hyperopt import Trials, STATUS_OK, tpe
from hyperas import optim
from hyperas.distributions import choice, uniform
from datetime import datetime as dt
from itertools import chain
import logging
from contextlib import redirect_stdout


def data_creation():
    """Placeholder function that eliminates kwargs from data creation.
    Default arguments have to be manually specified here, but cannot
    be passed into the parent function.
    ------------------------------------
    *As per hyperas documentation, relevant imports are defined
    within this function."""
    from src.features.build_features import training_generation, TRAIN, era_sampling
    small_data = era_sampling(TRAIN, 40)
    X_train, X_test, y_train, y_test = training_generation(data=small_data,
                                                           competition='bernie',
                                                           fraction=0.75,
                                                           polynomial=True)
    return X_train, X_test, y_train, y_test


def overfit_model():
    """Creates brute-force model as a "ground truth" for future
    classifiers.
    """
    X_train, X_test, y_train, y_test = data_creation()
    def layer_generator(x, n_layers):
       return Dense(n_layers, activation='tanh')(x)

    input_tensor = Input(shape=X_train.shape[1:])
    x = Dense(1024, activation='tanh')(input_tensor)
    twos = [512, 256, 128, 64]
    for i in chain(range(940, 256, -64), range(256, 23, -15),
                   range(23, 1, -3)):
        x = layer_generator(x, i)
    output = Dense(1, activation='sigmoid')(x)
    model = Model(input_tensor, output)
    adam = optimizers.Adam(lr=0.1)
    model.compile(optimizer='rmsprop',
                  loss='binary_crossentropy',
                  metrics=['acc'])
    print(model.summary())
    model.fit(X_train, y_train, epochs=10,
              batch_size=32, verbose=2,
              validation_data=(X_test, y_test))
    score, acc = model.evaluate(X_test, y_test, verbose=0)
    return score, acc


def create_model(X_train, X_test, y_train, y_test):
    """Instantiates neural network with hyperas random search optimizer.
    """
    input_tensor = Input(shape=X_train.shape[1:])
    x = Dense(62, activation={{choice(['relu', 'sigmoid', 'tanh'])}},
              kernel_regularizer={{
                  choice([None, regularizers.l2(0.01),regularizers.l2(0.1),
                          regularizers.l1(0.01),regularizers.l1(0.1)])
                                }})(input_tensor)
#   Batch normalization would be helpful here, but is an open issue
#   with hyperas (issue #125)
    x = Dense({{choice([894, 584, 429, 273, 229, 196, 170, 149])}},
              activation={{choice(['relu', 'sigmoid', 'tanh'])}},
              kernel_regularizer={{
                  choice([None, regularizers.l2(0.01), regularizers.l2(0.1),
                          regularizers.l1(0.01), regularizers.l1(0.1)])
              }})(x)
    x = Dense({{choice([1788, 1168, 858, 546, 458, 392, 340, 298])}},
              activation={{choice(['relu', 'sigmoid', 'tanh'])}},
              kernel_regularizer={{
                  choice([None, regularizers.l2(0.01), regularizers.l2(0.1),
                          regularizers.l1(0.01), regularizers.l1(0.1)])
              }})(x)
    x = Dropout({{uniform(0, 0.5)}})(x)
    x = Dense({{choice([894, 584, 429, 273, 229, 196, 170, 149])}},
              activation={{choice(['relu', 'sigmoid', 'tanh'])}},
              kernel_regularizer={{
                  choice([None, regularizers.l2(0.01), regularizers.l2(0.1),
                          regularizers.l1(0.01), regularizers.l1(0.1)])
              }}
              )(x)
    x = Dropout({{uniform(0, 0.5)}})(x)
    x = Dense(62, activation={{choice(['relu', 'sigmoid', 'tanh'])}},
              kernel_regularizer={{
                  choice([None, regularizers.l2(0.01), regularizers.l2(0.1),
                          regularizers.l1(0.01), regularizers.l1(0.1)])
              }}
              )(x)
    x = Dense(23, activation={{choice(['relu', 'sigmoid', 'tanh'])}},
              kernel_regularizer={{
                  choice([None, regularizers.l2(0.01), regularizers.l2(0.1),
                          regularizers.l1(0.01), regularizers.l1(0.1)])
              }}
              )(x)
    output = Dense(1, activation='sigmoid')(x)

    model = Model(input_tensor, output)
    # Callbacks are defined here, but should not be needed with 5 epochs.
    # Note that hyperas DOES NOT WORK with tensorboard callbacks.
    # callbacks_list = [callbacks.EarlyStopping(monitor='acc', patience=10),
    #                   callbacks.ModelCheckpoint(filepath='MLP.h5',
    #                                             monitor='val_loss',
    #                                             save_best_only=True)]
    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['acc'])
    model.fit(X_train, y_train, epochs=5,
              batch_size={{choice([32, 64, 128, 256, 512])}}, verbose=2,
              validation_data=(X_test, y_test))
    score, acc = model.evaluate(X_test, y_test, verbose=0)
    if score <= 0.6934:
        model.save(f'{dt.now().toordinal()}_{int(acc*100)}.h5')
    print(f'Test accuracy: {acc}')
    return {'loss': -acc, 'status': STATUS_OK}


def create_with_generator(X_train, X_test, y_train, y_test):
    """Testing to see if hyperas plays well with a layer generator function"""
    logging.basicConfig(filename='hyperas.log', level=logging.DEBUG)
    def layer_generator(x, n_layers, activation, regularizer):
        return Dense(n_layers, activation=activation,
                    kernel_regularizer=regularizer)(x)

    input_tensor = Input(shape=X_train.shape[1:])
    x = Dense(1024,
              activation={{choice(['tanh',
                                   'relu',
                                   'sigmoid'])}})(input_tensor)

    for i in chain(range(940, 256, {{choice([-76, -114, -228, -342])}}),
                   range(256, 23, {{choice([-18, -26, -39, -117])}}),
                   range(23, 1, -3)):
        x = layer_generator(x, i,
                            {{choice(['relu', 'sigmoid', 'tanh'])}},
                            {{choice([None, regularizers.l2(0.01),
                                            regularizers.l2(0.1),
                                            regularizers.l1(0.01),
                                            regularizers.l1(0.1)])}})
    output = Dense(1, activation='sigmoid')(x)
    model = Model(input_tensor, output)
    model.compile(optimizer={{choice([optimizers.Adam(lr=0.1),
                                      'adam', 'sgd', 'rmsprop',
                                      optimizers.sgd(nesterov=True),
                                      optimizers.sgd(lr=0.01),
                                      optimizers.sgd(lr=0.1, nesterov=True),
                                      optimizers.rmsprop(lr=0.01)],
                                     )}},
                  loss='binary_crossentropy',
                  metrics=['acc'])
    with open('modelsummary.txt', 'w') as f:
        with redirect_stdout(f):
            model.summary()
    try:
        model.fit(X_train, y_train, epochs=5,
                  batch_size={{choice([4, 16, 32, 64, 128])}}, verbose=2,
                  validation_data=(X_test, y_test))
    except Exception as e:
        logging.warning(e.__doc__)
        K.clear_session()
        pass

    score, acc = model.evaluate(X_test, y_test, verbose=0)
    logging.info(score, acc)
    if score <= 0.6934:
        model.save('{}.h5'.format(dt.now().strftime('%y%m%d_%H%M%S')))
        #Interesting note: f-string didn't work here. Possibly doesn't
        #like having strings within its brackets.
    return {'loss': -acc, 'status': STATUS_OK}


if __name__ == '__main__':
    # below statement copied from keras issue #2102
    # Will hopefully fix crashing midway through optimization
    cfg = K.tf.ConfigProto()
    cfg.gpu_options.allow_growth = True
    K.set_session(K.tf.Session(config=cfg))
    best_run, best_model = optim.minimize(model=create_with_generator,
                                          data=data_creation,
                                          algo=tpe.suggest,
                                          max_evals=5,
                                          trials=Trials())
    X_train, X_test, y_train, y_test = data_creation()

