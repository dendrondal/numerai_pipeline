import numerox as nx
import os
from pathlib import Path
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import learning_curve
from sklearn.linear_model import LogisticRegression

NXD = nx.data.Data
PROJECT_DIR = '/home/dal/bin/numerai'


def data_split(project_dir=PROJECT_DIR, do_pca=False):
    """Takes in HDF5 file generated in main.py, converts it into
    training data and pca data (90% variance as threshold)
     Returns training data and pca data dataframes."""
    raw_data = os.path.join(project_dir, 'data/raw/numerai_dataset.hdf')
    interim_path = os.path.join(project_dir, 'data/interim/')
    print('Loading data...')
    data = nx.load_data(raw_data)
    train = data['train']
    train.df.to_csv(interim_path + 'train.csv')
    #keep variables contributing to 90% of variance
    if do_pca:
        print('Starting PCA...')
        pca = data.pca(nfactor=0.9, data_fit=data['train'])
        pca.df.to_csv(interim_path + 'pca.csv')
        return train, pca
    else:
        return train



#Declaring data as a global variable for now, will later be encapsulated
TRAIN = data_split()


def distribution_recording(data: NXD):
    """Determines similarity of IQR measurements between eras.
    Idea here is to take summary statistics of top
    6 or 9 X values contributing to variance,
    and plot them vs. era. If there is large between-era
    variance, the era sampling function below may not
    be usable for this dataset."""
    reduced = data.pca(nfactor=9)
    fig, axes = plt.subplots(nrows=3, ncols=3)

    for era in reduced.df['era'].unique():
        for ax, pc in zip(np.ndenumerate(axes),
                          reduced.column_list(x_only=True)):
            df = reduced.df
            summary = df.loc[df.era == era, pc]
            axes[ax[0]].plot(era, summary[4], linestyle='--', c='r')
            axes[ax[0]].plot(era, summary[5], c='b')
            axes[ax[0]].plot(era, summary[6], linestyle='--', c='r')
    plt.savefig('/home/dal/bin/numerai/reports/figures/feature_IQR.png')


def training_generation(data=TRAIN, competition='bernie', fraction=0.7,
                        polynomial=False):
    """Makes train, test data from nx data"""
    y = data.y_df[competition].dropna()
    cleaned = data.loc[y.index.values]
    X = cleaned.x
    assert len(X) == len(y), f"X and y are unequal lengths: {len(X)} and {len(y)}"
    eras = set(cleaned.era)
    train_eras = np.random.choice(list(eras), int(len(eras)*fraction),
                                  replace=False)
    test_eras = eras - set(train_eras)
    train_data = cleaned.era_isin(train_eras)
    test_data = cleaned.era_isin(test_eras)
    if polynomial:
        print('Creating polynomial features...')
        X_train = polynomial_features(train_data)
        X_test = polynomial_features(test_data)
        print('Finished creating polynomial features')
    else:
        X_train = train_data.x
        X_test = test_data.x
    y_train = train_data.y_df[competition]
    y_test = test_data.y_df[competition]

    return X_train, X_test, y_train.values, y_test.values


def learning_curve_plot(data):
    """Takes in reduced data and runs logistic regression. Determines
    bias-variance tradeoff, and determines point of diminishing returns
    for sample sizes."""
    lr = LogisticRegression(penalty='l2', random_state=42)
    X_train, _, y_train, _ = data
    train_sizes, train_scores, test_scores =\
        learning_curve(estimator=lr, X=X_train, y=y_train,
                       train_sizes=np.linspace(0.01, 0.5, 25),
                       cv=10, n_jobs=-1, verbose=1)
    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)
    plt.plot(train_sizes, train_mean, color='b', marker='o',
             markersize=5, label='Training Accuracy')
    plt.fill_between(train_sizes, train_mean + train_std,
                     train_mean - train_std, alpha=0.5, color='b')
    plt.plot(train_sizes, test_mean, color='g', linestyle='--',
             marker='s', markersize=5, label='Validation Accuracy')
    plt.fill_between(train_sizes, test_mean + test_std,
                     test_mean - test_std, alpha=0.15, color='g')
    plt.grid()
    plt.xlabel('Number of training samples')
    plt.ylabel('Accuracy')
    plt.legend(loc='lower right')
    plt.ylim([0.5, 1.0])
    plt.savefig(os.path.join(PROJECT_DIR + '/reports/figures/learning_curve' ))
    return train_mean, train_std, test_mean, test_std


def era_sampling(data: NXD, number: int) -> NXD:
    """Samples random data eras from the pca dataframe,
    to allow smaller models to be tested and ruled out."""
    eras = np.random.choice(range(1,120), size=number)
    downsampled = data.era_isin([f'era{i}' for i in eras])
    return downsampled


def polynomial_features(data: NXD) -> np.ndarray:
    poly = PolynomialFeatures(2)
    return poly.fit_transform(data.x)

